<?php

// Pull in the NuSOAP code
require_once('nusoap.php');

// Create the client instance
$client = new nusoap_client('http://localhost/WebServiceNuSoap/Server.php?wsdl',true);

$parametros= array('nombre' => 'Pedro', 'apellido' => 'Perez');
// Check for an error
$err = $client->getError();


if ($err) {
   // Display the error
   echo '<h2>Constructor error</h2><pre>' . $err . '</pre>';
   // At this point, you know the call that follows will fail
}

// Call the CurrencyConverter SOAP method
$result = $client->call('HolaMundo',$parametros);

// Check for a fault
if ($client->fault) {
   echo '<h2>Fault</h2><pre>';
   print_r($result);
   echo '</pre>';
}else{
      // Check for errors
      $err = $client->getError();
      if ($err) {
         // Display the error
         echo '<h2>Error</h2><pre>' . $err . '</pre>';
      } else {
         // Display the result
         echo '<h2>Result</h2><pre>';
         print_r($result);
         echo '</pre>';
      }
}