<?php 

require_once("nusoap.php");

$urn= "urn:miserviciowsdl";

$server= new soap_server();
$server->configureWSDL("MiWebService", $urn);
$server->schemaTargetNamespace = $urn;

function HolaMundo($nombre, $apellido){

	$info= "Hola ".$nombre. " Bienvenido a mi Web Service";
	return $info;
}

function Suma ($val1,$val2){

	$result= $val1+$val2;
	return $result;
}

$server->register(
	'HolaMundo',
	array('nombre' => 'xsd:string', 'apellido' => 'xsd:string'),
	array('return' => 'xsd:string'),
	$urn
);

$server->register(
	'Suma',
	array('val1' => 'xsd:integer', 'val2' => 'xsd:integer'),
	array('return' => 'xsd:integer'),
	$urn
);


$HTTP_RAW_POST_DATA = isset($GLOBALS['HTTP_RAW_POST_DATA']) ? $GLOBALS['HTTP_RAW_POST_DATA'] : '';

// $server->service($HTTP_RAW_POST_DATA);

$server->service(file_get_contents("php://input"));
